@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Overview</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="kt-section">
                            <div class="kt-section__content">

                                <div id="response">
                                    API response here
                                </div>

                                <script>
                                    fetch('api/klanten', {
                                        headers: {
                                            'x-csrf-token' : document.querySelector("meta[name='csrf-token']").getAttribute("content")
                                        }
                                    })
                                        .then(function(response) {
                                            response.json().then(function(data) {
                                                var str = JSON.stringify(data.cars);
                                                document.getElementById('response').innerHTML = str;
                                            });
                                        })
                                        .catch(function(err) {
                                            console.log('Error: ' +  err);
                                        });
                                </script>


{{--                                    // fetch('api/klanten')--}}
{{--                                    //     .then(function(response) {--}}
{{--                                            // response.json().then(function(data) {--}}
{{--                                            //    console.log(data);--}}
{{--                                            // });--}}
{{--                                            // response.json().then(function(data) {--}}
{{--                                            //     var str = JSON.stringify(data.cars);--}}
{{--                                            //     document.getElementById('response').innerHTML = str;--}}
{{--                                            // });--}}
{{--                                        // })--}}
{{--                                        // .catch(function(err) {--}}
{{--                                        //     console.log('Error: ' +  err);--}}
{{--                                        // });--}}

{{--                                <script>--}}
{{--                                    fetch('api/klanten', {--}}
{{--                                        headers: {--}}
{{--                                            'x-csrf-token' : document.querySelector("meta[name='csrf-token']").getAttribute("content")--}}
{{--                                        }--}}
{{--                                    })--}}
{{--                                    .then(function(response) {--}}
{{--                                        response.json().then(function(data) {--}}
{{--                                            var str = JSON.stringify(data.klanten);--}}
{{--                                            document.getElementById('response').innerHTML = str;--}}
{{--                                        });--}}
{{--                                    })--}}
{{--                                    .catch(function(err) {--}}
{{--                                        console.log('Error: ' +  err);--}}
{{--                                    });--}}
{{--                                </script>--}}

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
