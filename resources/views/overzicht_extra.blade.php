<base href="../../../">

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Overview</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="kt-section">
                            <div class="kt-section__content">

                                <table id="overzichtTable" class="table table-hover">
                                    <thead style="display:none">
                                        <tr class="d-flex">
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="d-flex">
                                            <th class="col-4"> Invoice ID </th>
                                            <td class="col-7"> {{ $information['factuur_id'] }} </td>
                                        </tr>
                                        <tr class="d-flex">
                                            <th class="col-4"> Date </th>
                                            <td class="col-7"> <?= date('m-d-Y', strtotime($information['datum'] )) ?> </td>
                                        </tr>
                                        <tr class="d-flex">
                                            <th class="col-4"> Status </th>
                                            <?php
                                                if($information['status'])
                                                    echo("<td class='bg-success col-7'>Paid</td>");
                                                else
                                                    echo("<td class='bg-warning col-7'>Waiting</td>");
                                            ?>
                                        </tr>
                                        <tr class="d-flex">
                                            <th class="col-4"> Total Price </th>
                                            <td class="col-7"> € {{ $information['bedrag'] }} </td>
                                        </tr>
                                        <tr class="d-flex">
                                            <th class="col-4"> &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp Single Price </th>
                                            <td class="col-7"> € {{ $information['Bedrag'] }} </td>
                                        </tr>
                                        <tr class="d-flex">
                                            <th class="col-4"> &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp Amount </th>
                                            <td class="col-7"> {{ $information['Aantal'] }} </td>
                                        </tr>
                                        <tr class="d-flex">
                                            <th class="col-4"> &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp VAT </th>
                                            <td class="col-7"> {{ $information['BTW'] }} % </td>
                                        </tr>
                                        <tr class="d-flex">
                                            <th class="col-4"> &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp Discount </th>
                                            <td class="col-7"> {{ $information['Korting'] }} % </td>
                                        <tr class="d-flex">
                                            <th class="col-4"> Description </th>
                                            <td class="col-7"> {{ $information['omschrijving'] }} </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <script>
                                    // makes the invoice table sortable
                                    $(document).ready(function () {
                                        $('#overzichtTable').DataTable({
                                            'paging': false,
                                            'searching': false,
                                            'bInfo': false,
                                            'ordering': false,
                                        });
                                        $('.dataTables_length').addClass('bs-select');
                                    });

                                    // // makes a row in the invoice table clickable
                                    // jQuery(document).ready(function($) {
                                    //     $(".clickable-row").click(function() {
                                    //         window.location = $(this).data("href");
                                    //     });
                                    // });
                                </script>

                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-1">
                        <a href="/overview">
                            <div class="btn btn-brand btn-elevate kt-login__btn-primary">
                                Back
                            </div>
                        </a>
                    </div>
                    <div class="col-3">
                        <a href="/overview">
                            <div class="btn btn-brand btn-elevate kt-login__btn-primary">
                                Download Invoice
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
