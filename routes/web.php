<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/home', 'HomeController@updatePassword')->name('resetPassword');

Route::get('/overview', 'HomeController@overzicht')->name('overzicht');

Route::get('/overview/{factuur_id}', ['uses' => 'HomeController@showExtraOverzicht']);

Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/klanten', function() {
    return view('klanten');
});
