<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\Hash;
use function PHPSTORM_META\type;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function overzicht(){

        $user = $user = Auth::user();
        $facturen = DB::select('select factuur_id, datum, bedrag, status from Factuur where user_id = ' . $user->id);

        return View::make('overzicht')->with( 'facturen', (array)$facturen );
    }

    public function updatePassword(Request $request){

        $user = $user = Auth::user();
        $email = $user->email;
        $password = $request->input('password');
        $password_confirm = $request->input('password_confirmation');

        $succesfull = false;

        if($password === $password_confirm){
            $user->password = Hash::make($password);
            $user->save();
            $succesfull = true;
        }

        return view('home')->with('passwordReset', $succesfull);
    }

    public function showExtraOverzicht($factuur_id){

        $user = $user = Auth::user();
        $factuur = DB::select("select factuur_id, user_id, datum, bedrag, status from Factuur where factuur_id = $factuur_id AND user_id = $user->id")[0];
        $factuurExtra = DB::select("select ID, Parent_id, omschrijving, Aantal, Bedrag, Regel_totaal, BTW, Korting from Extra_Factuur where Parent_id = $factuur_id")[0];

        if($user->id != $factuur->user_id){
            echo("This overview is not yours");
            die();
        }

        // 0 = wachtende
        // 1 = uitbetaald
        unset( $factuurExtra->ID );
        unset( $factuurExtra->Parent_id );

        $output = array_merge( json_decode( json_encode( $factuur ), true ), json_decode( json_encode( $factuurExtra ), true ));

        return view('overzicht_extra')->with( 'information', (array)$output);
    }
}
